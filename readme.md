End-to-End tests using [Cypress.io](http://cypress.io)

# Clone

```bash
git clone https://bitbucket.org/fliptight/doctorlogic-e2e-tests.git
```

# Setup

go to folder after cloning

```bash
cd doctorlogic-e2e-tests
```

install npm modules

```bash
npm install
```

# Launch the Test Runner

```bash
node_modules/.bin/cypress open
```