describe('Contact Us form tests', function () {
    var submitButton = 'button[type=submit]';
    var fullNameField = '#FullName';
    var phoneField = '#Phone';
    var emailField = '#Email';
    var commentField = '#Comment';

    var fullNameError = '.field-validation-error[data-valmsg-for=FullName]';
    var phoneError = '.field-validation-error[data-valmsg-for=Phone]';
    var emailError = '.field-validation-error[data-valmsg-for=Email]';

    beforeEach(function () {
        cy.visit('https://qa.doctorlogicsitestest.com/Contact-Us');
    });

    it('should not allow an empty form to be submitted', function() {
        cy.get(submitButton).click();

        cy.get('.field-validation-error').should('exist');

        cy.get(fullNameError).should('contain', 'The FullName field is required.');
        cy.get(phoneError).should('contain', 'The Phone field is required.');
        cy.get(emailError).should('contain', 'The Email field is required.');
    });

    it('should require the Full Name to have two separate words', function() {
        cy.get(fullNameField).type('qa');
        cy.get(submitButton).click();

        cy.get(fullNameError).should('contain', 'Please include both a first and a last name.');
    });

    it('should not let the phone number have letters', function() {
        cy.get(phoneField).type('214777abcd');
        cy.get(submitButton).click();

        cy.get(phoneError).should('contain', 'Please include a valid Phone Number.');
    });

    it('should not allow a phone with less than 7 digits', function() {
        cy.get(phoneField).type('214777');
        cy.get(submitButton).click();

        cy.get(phoneError).should('contain', 'Please include a valid Phone Number.');
    });

    it('should have some email validation', function() {
        cy.get(emailField).type('a');
        cy.get(submitButton).click();

        cy.get(emailError).should('contain', 'The Email field is not a valid e-mail address.');
    });

    it('should have no errors when filling out a valid form', function() {
        cy.get(fullNameField).type('qa tester');
        cy.get(phoneField).type('2147770000');
        cy.get(emailField).type('a@b.com');

        cy.get('.field-validation-error').should('not.exist');
    });

    it('should submit and go to the thank you page with correct details', function() {
        cy.get(fullNameField).type('qa tester');
        cy.get(phoneField).type('2147770000');
        cy.get(emailField).type('a@b.com');
        cy.get(commentField).type('Cypress E2E - please disregard');
        cy.get(submitButton).click();

        cy.url().should('contain', 'Thank-You');

        cy.get('.confirmation-info > .table > span:nth-child(1) > dd').should('contain', 'qa tester');
        cy.get('.confirmation-info > .table > span:nth-child(2) > dd').should('contain', 'a@b.com');
        cy.get('.confirmation-info > .table > span:nth-child(3) > dd').should('contain', '2147770000');
        cy.get('.comments').should('contain', 'Cypress E2E - please disregard');
    });
});
