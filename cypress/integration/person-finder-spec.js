describe('Person Finder tests', function () {
    var advancedSearchToggle = 'button.advanced-search-toggle';
    var advancedSearchSection = '#advanced-search-collapse';
    var nameField = '#name';
    var searchButton = 'button[type=submit]';
    var viewMoreButton = '#view-more-post';
    var viewAllLink = '.advanced-search-reset';

    var unfilteredCount = 'span.unfiltered-count';
    var filteredCount = 'span.filtered-count';
    
    var gridMode = '.grid-style.btn';
    var listMode = '.list-style.btn';
    var displayedItem = '.finder-list-item';

    beforeEach(function () {
        cy.visit('https://gnyacd.doctorlogicsitestest.com/Members/Grid');
    });

    context('Grid Mode', function() {

        it('should have grid view selected by default', function() {
            cy.get(listMode).should('not.have.class', 'active');
            cy.get(gridMode).should('have.class', 'active');
            cy.get('body').should('have.class', 'finderlist-grid');
        });

        it('should be paginated to 12 items at a time in grid mode', function() {
            cy.get(displayedItem).should('have.length', 12);
        });

        it('should display 24 items after clicking on view more in grid mode', function() {
            cy.get(viewMoreButton).click();
            cy.get(displayedItem).should('have.length', 24);
        });
    });

    context('List Mode', function() {

        it('should switch to list view when list view button is clicked', function() {
            cy.get(listMode).click();

            cy.get(gridMode).should('not.have.class', 'active');
            cy.get(listMode).should('have.class', 'active');
            cy.get('body').should('have.class', 'finderlist-list');
        });

        it('should be paginated to  12 items at a time in list view', function() {
            cy.get(listMode).click();
            cy.get(displayedItem).should('have.length', 12);
        });

        it('should display 24 items after clicking on view more in list mode', function() {
            cy.get(listMode).click();
            cy.get(viewMoreButton).click();
            cy.get(displayedItem).should('have.length', 24);
        });
    });

    context('Advanced Search', function() {

        it('should have advanced search hidden by default', function() {
            cy.get(advancedSearchSection).should('not.be.visible');
        });

        it('should show advanced search when toggled open', function() {
            cy.get(advancedSearchToggle).click();
            cy.get(advancedSearchSection).should('be.visible');

        });

        it('should display only 39 count in the header when no search filters', function() {
            cy.get(unfilteredCount).should('contain', '39');
            cy.get(filteredCount).should('not.exist');
        });

        it('should display 8 out of 39 when searching for jo', function() {
            cy.get(advancedSearchToggle).click();
            cy.get(nameField).type('jo');
            cy.get(searchButton).click();

            cy.get(filteredCount).should('contain', '8');
            cy.get(unfilteredCount).should('contain', '39');
            cy.get(displayedItem).should('have.length', 8);
        });

        it('should reset back to 39 items when clicking on View All', function() {
            cy.get(advancedSearchToggle).click();
            cy.get(nameField).type('jo');
            cy.get(searchButton).click();
            cy.get(viewAllLink).click();

            cy.get(unfilteredCount).should('contain', '39');
            cy.get(filteredCount).should('not.exist');
            cy.get(displayedItem).should('have.length', 39);
        });
    });
});
